package com.epam.mentorship.algorithm;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {

        int[] array1 = {-4, 10, -2, 0, 1, -20, 25, 67};
        printArray(array1);
        bubbleSort(array1);
        printArray(array1);

        System.out.println("#################################################");

        int[] array2 = {34, -6, 10, 0, 5, 38, -33, 50, -128, 45};
        printArray(array2);
        quickSort(array2);
        printArray(array2);

    }

    private static void printArray(int[] array) {
        final String result = Arrays.stream(array)
                .mapToObj(Objects::toString)
                .collect(Collectors.joining(" "));
        System.out.println(result);
    }

    private static void bubbleSort(int[] array) {
        if (Objects.isNull(array) || array.length <= 0) {
            return;
        }

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int x = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = x;
                }
            }
        }
    }

    private static void quickSort(int[] array) {
        if (Objects.isNull(array) || array.length <= 0) {
            return;
        }

        quickSort(0, array.length - 1, array);
    }

    private static void quickSort(int low, int high, int[] array) {
        int i = low;
        int j = high;

        int pivot = array[low + (high - low) / 2];

        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j) {
            quickSort(low, j, array);
        }
        if (i < high) {
            quickSort(i, high, array);
        }
    }

}

