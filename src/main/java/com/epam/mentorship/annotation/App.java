package com.epam.mentorship.annotation;

import com.epam.mentorship.annotation.model.Creature;

import java.util.List;

/**
 * Created by Liliia Labinska on 2/13/2018
 */

public class App {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        final List<Class<?>> classesImplementing =
                ReflectionUtils.findClassesImplementing(Creature.class, Creature.class.getPackage());

        System.out.println("\n########################################################## \n" +
                "Classes wich implementing Creature interface: \n" +
                "##########################################################");
        classesImplementing.forEach(System.out::println);

        List<Class<?>> classesAnnotated = ReflectionUtils.findClassesAnnotated(classesImplementing, MyAnnotation.class);

        System.out.println("\n########################################################## \n" +
                "Classes wich annotated with @MyAnnotation: \n" +
                "##########################################################");
        classesAnnotated.forEach(System.out::println);


        Class<?> calcClass = Class.forName("Human");
        Creature api = (Creature)calcClass.newInstance();
// Use it

    }

}
