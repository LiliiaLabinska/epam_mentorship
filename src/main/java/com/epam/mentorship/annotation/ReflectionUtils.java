package com.epam.mentorship.annotation;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

/**
 * Created by Liliia Labinska on 2/13/2018
 */

public class ReflectionUtils {

    public static List<Class<?>> findClassesAnnotated(List<Class<?>> classes, Class annotation) {
        List<Class<?>> result = new ArrayList<>();

        for (final Class<?> aClass : classes) {
            if (aClass.isAnnotationPresent(annotation)) {
                result.add(aClass);
            }
        }

        return result;
    }

    public static List<Class<?>> findClassesImplementing(final Class<?> interfaceClass, final Package fromPackage) {

        if (Objects.isNull(interfaceClass)) {
            return Collections.emptyList();
        }

        if (Objects.isNull(fromPackage)) {
            return Collections.emptyList();
        }

        final List<Class<?>> result = new ArrayList<>();
        try {
            final Class<?>[] targets = getAllClassesFromPackage(fromPackage.getName());

            Arrays.stream(targets)
                    .filter(Objects::nonNull)
                    .filter(aTarget -> !aTarget.equals(interfaceClass))
                    .filter(interfaceClass::isAssignableFrom)
                    .forEach(result::add);

        } catch (ClassNotFoundException | IOException e) {
            System.err.println(e);
        }

        return result;
    }

    public static Class[] getAllClassesFromPackage(final String packageName) throws ClassNotFoundException, IOException {
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        assert classLoader != null;

        final String path = packageName.replace('.', '/');
        final Enumeration<URL> resources = classLoader.getResources(path);
        final List<File> directories = new ArrayList<>();

        while (resources.hasMoreElements()) {
            final URL resource = resources.nextElement();
            directories.add(new File(resource.getFile()));
        }

        final ArrayList<Class> classes = new ArrayList<>();

        directories.forEach(directory -> {
            try {
                classes.addAll(findClasses(directory, packageName));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });


        return classes.toArray(new Class[classes.size()]);
    }

    public static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {

        if (!directory.exists()) {
            return Collections.emptyList();
        }

        final File[] files = directory.listFiles();

        if (Objects.isNull(files)) {
            return Collections.emptyList();
        }

        final List<Class<?>> classes = new ArrayList<>();

        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

}
