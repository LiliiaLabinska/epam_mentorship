package com.epam.mentorship.annotation.model;

import com.epam.mentorship.annotation.MyAnnotation;

/**
 * Created by Liliia Labinska on 2/13/2018
 */

@MyAnnotation
public class Animal implements Creature {

    @Override
    public void saySomething() {
        System.out.println("Grrrrr, I'm an animal!");
    }
}
