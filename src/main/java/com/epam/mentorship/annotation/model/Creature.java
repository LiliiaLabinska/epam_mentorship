package com.epam.mentorship.annotation.model;

/**
 * Created by Liliia Labinska on 2/13/2018
 */

public interface Creature {

    void saySomething();

}
