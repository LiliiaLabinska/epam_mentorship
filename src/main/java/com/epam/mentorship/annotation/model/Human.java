package com.epam.mentorship.annotation.model;

import com.epam.mentorship.annotation.MyAnnotation;

/**
 * Created by Liliia Labinska on 2/13/2018
 */

@MyAnnotation
public class Human implements Creature {

    @Override
    public void saySomething() {
        System.out.println("Hello, I'm a human!");
    }
}
