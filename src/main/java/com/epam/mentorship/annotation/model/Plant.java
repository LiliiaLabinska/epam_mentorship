package com.epam.mentorship.annotation.model;

/**
 * Created by Liliia Labinska on 2/13/2018
 */

public class Plant implements Creature {

    @Override
    public void saySomething() {
        System.out.println("Shhhhhh, I'm  plant shhhhh...");
    }
}
