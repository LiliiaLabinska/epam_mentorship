package com.epam.mentorship.bitwise;

public class App {

    public static void main(String[] args) {
        int a = 60; /* 60 = 0011 1100 */
        int b = 13;	/* 13 = 0000 1101 */
        int c;

        c = a & b;        /* 1 + 1 = 1 */
        System.out.println("a & b = " + c );

        c = a | b;        /* 1 + 1 = 1; 0 + 1 = 1*/
        System.out.println("a | b = " + c );

        c = a ^ b;        /* 1 + 0 = 1 */
        System.out.println("a ^ b = " + c );

        c = ~a;           /* 1 = 0; 0 = 1 */
        System.out.println("~a = " + c );

        c = a << 2;       /* value << count; 0011 1100 = 1111 0000 */
        System.out.println("a << 2 = " + c );

        c = a >> 2;       /* value << count; 0011 1100 = 1111 */
        System.out.println("a >> 2  = " + c );

        c = a >>> 2;      /* -//- (without digest) 0011 1100 = 0000 1111 */
        System.out.println("a >>> 2 = " + c );
    }

}
