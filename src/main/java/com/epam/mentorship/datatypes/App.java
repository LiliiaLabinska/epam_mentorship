package com.epam.mentorship.datatypes;

import java.math.BigInteger;

public class App {

    public static void main(String[] args) {

        System.out.println(5/2);
        System.out.println(5/2.0);

        String s = "Hello, world!";
        System.out.println(s);

        // to bits
        System.out.println(new BigInteger(s.getBytes()).toString(2));

        // to octal
        System.out.println(new BigInteger(s.getBytes()).toString(8));

        // to hex
        System.out.println(new BigInteger(s.getBytes()).toString(16));

        System.out.println(isPalindrom(s));

    }

    public static boolean isPalindrom(String s) {
        int i = 0;
        int j = s.length() - 1;

        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
            ++i;
            --j;
        }

        return true;
    }

}
