package com.epam.mentorship.innerclasses;

public class App {

    public static void main(String[] args) {

        MyClass.MyStaticInnerClass staticInnerClass = new MyClass.MyStaticInnerClass();

        MyClass myClass = new MyClass();

        MyClass.MyNonStaticInnerClass nonStaticInnerClass = myClass.new MyNonStaticInnerClass();
    }

}
