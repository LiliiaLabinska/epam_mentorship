package com.epam.mentorship.innerclasses;

public class MyClass {

    private int nonStaticField;

    private static int staticField;

    static class MyStaticInnerClass {

        public void someMethod() {
            System.out.println(staticField);
        }

    }

    class MyNonStaticInnerClass {
        public void someMethod() {
            System.out.println(nonStaticField);
            System.out.println(staticField);
        }
    }

}
