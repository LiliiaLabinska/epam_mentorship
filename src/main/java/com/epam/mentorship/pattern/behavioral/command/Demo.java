package com.epam.mentorship.pattern.behavioral.command;

import com.epam.mentorship.pattern.behavioral.command.editor.Editor;

/**
 * Created by Liliia Labinska on 1/30/2018
 */

public class Demo {
    public static void main(String[] args) {
        Editor editor = new Editor();
        editor.init();
    }
}
