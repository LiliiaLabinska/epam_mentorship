package com.epam.mentorship.pattern.behavioral.command.commands;

import com.epam.mentorship.pattern.behavioral.command.editor.Editor;

/**
 * Created by Liliia Labinska on 1/30/2018
 */

public abstract class Command {

    public Editor editor;
    protected String backup;

    public Command(Editor editor) {
        this.editor = editor;
    }

    protected void backup() {
        backup = editor.textField.getText();
    }

    public void undo() {
        editor.textField.setText(backup);
    }

    public abstract Boolean execute();
}
