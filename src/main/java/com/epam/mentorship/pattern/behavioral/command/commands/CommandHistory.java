package com.epam.mentorship.pattern.behavioral.command.commands;

import java.util.Stack;

/**
 * Created by Liliia Labinska on 1/30/2018
 */

public class CommandHistory {

    private Stack history = new Stack();

    public void push(Command c) {
        history.push(c);
    }

    public Command pop() {
        return (Command) history.pop();
    }

    public Boolean isEmpty() { return history.isEmpty(); }

}
