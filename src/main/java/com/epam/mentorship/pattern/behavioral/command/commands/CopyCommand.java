package com.epam.mentorship.pattern.behavioral.command.commands;

import com.epam.mentorship.pattern.behavioral.command.editor.Editor;

/**
 * Created by Liliia Labinska on 1/30/2018
 */

public class CopyCommand extends Command{

    public CopyCommand(Editor editor) {
        super(editor);
    }

    @Override
    public Boolean execute() {
        editor.clipboard = editor.textField.getSelectedText();
        return false;
    }

}
