package com.epam.mentorship.pattern.behavioral.command.commands;

import com.epam.mentorship.pattern.behavioral.command.editor.Editor;

/**
 * Created by Liliia Labinska on 1/30/2018
 */

public class PasteCommand extends Command {

    public PasteCommand(Editor editor) {
        super(editor);
    }

    @Override
    public Boolean execute() {
        if (editor.clipboard.isEmpty()) return false;

        backup();
        editor.textField.append(editor.clipboard);
        return true;
    }

}
