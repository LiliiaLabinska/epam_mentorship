package com.epam.mentorship.pattern.behavioral.observer;

import com.epam.mentorship.pattern.behavioral.observer.editor.Editor;
import com.epam.mentorship.pattern.behavioral.observer.listeners.EmailNotificationListener;
import com.epam.mentorship.pattern.behavioral.observer.listeners.LogOpenListener;

/**
 * Created by Liliia Labinska on 1/31/2018
 */

public class Demo {

    public static void main(String[] args) {
        Editor editor = new Editor();
        editor.events.subscribe("open", new LogOpenListener("/path/to/log/file.txt"));
        editor.events.subscribe("save", new EmailNotificationListener("admin@example.com"));

        try {
            editor.openFile("test.txt");
            editor.saveFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
