package com.epam.mentorship.pattern.behavioral.observer.listeners;

import java.io.File;

/**
 * Created by Liliia Labinska on 1/31/2018
 */

public interface EventListener {

    void update(String eventType, File file);

}
