package com.epam.mentorship.pattern.behavioral.observer.listeners;

import java.io.File;


/**
 * Created by Liliia Labinska on 1/31/2018
 */

public class LogOpenListener implements EventListener{

    private File log;

    public LogOpenListener(String fileName) {
        this.log = new File(fileName);
    }

    @Override
    public void update(String eventType, File file) {
        System.out.println("Save to log " + log + ": Someone has performed " + eventType + " operation with the following file: " + file.getName());
    }

}
