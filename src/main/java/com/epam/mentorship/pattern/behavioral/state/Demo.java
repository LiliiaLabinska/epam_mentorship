package com.epam.mentorship.pattern.behavioral.state;

import com.epam.mentorship.pattern.behavioral.state.ui.Player;
import com.epam.mentorship.pattern.behavioral.state.ui.UI;

/**
 * Created by Liliia Labinska on 2/1/2018
 */

public class Demo {

    public static void main(String[] args) {
        Player player = new Player();
        UI ui = new UI(player);
        ui.init();
    }
    
}
