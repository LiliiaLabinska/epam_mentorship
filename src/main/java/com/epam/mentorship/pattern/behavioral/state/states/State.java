package com.epam.mentorship.pattern.behavioral.state.states;

import com.epam.mentorship.pattern.behavioral.state.ui.Player;

/**
 * Created by Liliia Labinska on 2/1/2018
 */

public abstract class State {

    public Player player;

    public State(Player player) {
        this.player = player;
    }

    public abstract String onLock();
    public abstract String onPlay();
    public abstract String onNext();
    public abstract String onPrevious();
}
