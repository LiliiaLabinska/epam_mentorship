package com.epam.mentorship.pattern.create.factory;

public interface Logistics {

    Transport createTransport();
}
