package com.epam.mentorship.pattern.create.factory;

public class RoadLogistics implements Logistics {

    public Transport createTransport() {
        return new Truck();
    }
}
