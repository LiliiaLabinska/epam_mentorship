package com.epam.mentorship.pattern.create.factory;

public class SeaLogistics implements Logistics {

    public Transport createTransport() {
        return new Ship();
    }
    
}
