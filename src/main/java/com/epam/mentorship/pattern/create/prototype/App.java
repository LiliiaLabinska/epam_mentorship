package com.epam.mentorship.pattern.create.prototype;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {
        List<Shape> shapes = new ArrayList();
        List<Shape> shapesCopy = new ArrayList();

        Circle circle = new Circle();

        circle.setRadius(5);
        circle.setColor("red");
        circle.setX(2);
        circle.setY(4);

        shapes.add(circle);

        Circle anotherCircle = (Circle) circle.clone();
        shapes.add(anotherCircle);

        cloneAndCompare(shapes, shapesCopy);
    }

    private static void cloneAndCompare(List<Shape> shapes, List<Shape> shapesCopy) {
        for (Shape shape : shapes) {
            shapesCopy.add(shape.clone());
        }

        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i) != shapesCopy.get(i)) {
                System.out.println(i + ": Shapes are different objects (yay!)");
                if (shapes.get(i).equals(shapesCopy.get(i))) {
                    System.out.println(i + ": And they are identical (yay!)");
                } else {
                    System.out.println(i + ": But they are not identical (booo!)");
                }
            } else {
                System.out.println(i + ": Shape objects are the same (booo!)");
            }
        }
    }

}
