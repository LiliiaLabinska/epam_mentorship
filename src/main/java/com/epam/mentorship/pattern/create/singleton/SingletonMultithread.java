package com.epam.mentorship.pattern.create.singleton;

import java.util.Objects;

public final class SingletonMultithread {

    private static volatile SingletonMultithread instance;
    private String value;

    private SingletonMultithread(final String value) {
        this.value = value;
    }

    public static SingletonMultithread getInstance(String value) {
        if (instance == null) {
            synchronized (SingletonMultithread.class) {
                if (Objects.isNull(instance)) {
                    instance = new SingletonMultithread(value);
                }
            }
        }
        return instance;
    }
}
