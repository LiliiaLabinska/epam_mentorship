package com.epam.mentorship.pattern.structure.bridge;

import com.epam.mentorship.pattern.structure.bridge.device.Device;
import com.epam.mentorship.pattern.structure.bridge.device.Radio;
import com.epam.mentorship.pattern.structure.bridge.device.Tv;
import com.epam.mentorship.pattern.structure.bridge.remote.AdvancedRemote;
import com.epam.mentorship.pattern.structure.bridge.remote.BasicRemote;

public class Demo {

    public static void main(String[] args) {
        testDevice(new Tv());
        testDevice(new Radio());
    }

    public static void testDevice(Device device) {
        System.out.println("Tests with basic remote.");
        BasicRemote basicRemote = new BasicRemote(device);
        basicRemote.power();
        device.printStatus();

        System.out.println("Tests with advanced remote.");
        AdvancedRemote advancedRemote = new AdvancedRemote(device);
        advancedRemote.power();
        advancedRemote.mute();
        device.printStatus();
    }

}
