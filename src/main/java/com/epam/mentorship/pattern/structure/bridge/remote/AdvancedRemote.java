package com.epam.mentorship.pattern.structure.bridge.remote;

import com.epam.mentorship.pattern.structure.bridge.device.Device;

public class AdvancedRemote extends BasicRemote {

    public AdvancedRemote(Device device) {
        super.device = device;
    }

    public void mute() {
        System.out.println("Remote: mute");
        device.setVolume(0);
    }

}
