package com.epam.mentorship.pattern.structure.bridge.remote;

public interface Remote {

    void power();

    void volumeDown();

    void volumeUp();

    void channelDown();

    void channelUp();

}
