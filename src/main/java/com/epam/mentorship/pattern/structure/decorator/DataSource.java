package com.epam.mentorship.pattern.structure.decorator;

public interface DataSource {

    void writeData(String data);

    String readData();

}
