package com.epam.mentorship.serialization;

import java.io.*;

public class App {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Employee employee = new Employee();
        employee.setName("Reyan Ali");
        employee.setAddress("Phokka Kuan, Ambehta Peer");
        employee.setSSN(11122333);
        employee.setNumber(101);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ObjectOutputStream ous = new ObjectOutputStream(baos);
        ous.writeObject(employee);
        ous.close();

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);

        Employee cloneEmployee = (Employee) ois.readObject();
    }

    public void serialize(Employee e) {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("/tmp/employee.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(e);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in /tmp/employee.ser");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public void deserialize() {
        Employee e;
        try {
            FileInputStream fileIn = new FileInputStream("/tmp/employee.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (Employee) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Employee class not found");
            c.printStackTrace();
            return;
        }
    }


}
