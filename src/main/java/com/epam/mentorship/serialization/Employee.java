package com.epam.mentorship.serialization;

import java.io.Serializable;

public class Employee implements Serializable {

    private String name;
    private String address;
    private transient int SSN;
    private int number;

    public void mailCheck() {
        System.out.println("Mailing a check to " + name + " " + address);
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public int getSSN() {
        return SSN;
    }

    public void setSSN(final int SSN) {
        this.SSN = SSN;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(final int number) {
        this.number = number;
    }
}
