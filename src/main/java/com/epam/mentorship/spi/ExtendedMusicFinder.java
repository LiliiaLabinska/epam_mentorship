package com.epam.mentorship.spi;

import java.util.Collections;
import java.util.List;

public class ExtendedMusicFinder implements MusicFinder{

    @Override
    public List<String> getMusic() {
        return Collections.singletonList("From ExtendedMusicFinder...");
    }

}
