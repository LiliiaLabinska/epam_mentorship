package com.epam.mentorship.spi;

import java.util.List;

public interface MusicFinder {

    List<String> getMusic();

}
