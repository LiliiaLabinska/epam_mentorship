package com.epam.mentorship.spi;

import java.util.Collections;
import java.util.List;

public class MyMusicFinder implements MusicFinder {

    @Override
    public List<String> getMusic() {
        return Collections.singletonList("From MyMusicFinder...");
    }

}
